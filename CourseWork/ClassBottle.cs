﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CourseWork
{
    public class Bottle
    {
        public PictureBox PictureBox { get; }
        public PointF Velocity { get; set; }
        public bool IsLarge { get; }
        public bool IsRight { get; }
        public float Angle { get; set; }
        public Image OriginalImage { get; }
        public CircleHitbox Hitbox { get; set; }

        public Bottle(PictureBox pictureBox, PointF velocity, bool isLarge, bool isRight)
        {
            PictureBox = pictureBox;
            Velocity = velocity;
            IsLarge = isLarge;
            Angle = 0;
            OriginalImage = pictureBox.Image;
            IsRight = isRight;

            this.Hitbox = new CircleHitbox(new Point((int)pictureBox.Location.X + pictureBox.Width / 2, (int)pictureBox.Location.Y + pictureBox.Height / 2), Math.Min(pictureBox.Width, pictureBox.Height) / 2);
        }

        public void UpdatePosition(Point newCenter)
        {
            this.Hitbox.UpdatePosition(newCenter);
            this.PictureBox.Location = new Point(newCenter.X - this.PictureBox.Width / 2, newCenter.Y - this.PictureBox.Height / 2);
        }
    }
}
