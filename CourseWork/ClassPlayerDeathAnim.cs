﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using CourseWork.Properties;

public class HeartAnimation
{
    private PictureBox playerHeart;
    private List<PictureBox> fragments = new List<PictureBox>();
    private Timer timer = new Timer();
    private int timeStep = 0;
    private List<PointF> velocities = new List<PointF>();
    private List<PointF> positions = new List<PointF>();
    private float gravity = 0.5f;
    private Label gameOverLabel;
    private Label restartLabel;

    public HeartAnimation(PictureBox heart, int param)
    {
        playerHeart = heart;
        InitializeFragments(param);
        InitializeLabels();
    }

    private void InitializeFragments(int param)
    {
        Random rnd = new Random();
        for (int i = 0; i < 5; i++)
        {
            PictureBox fragment = new PictureBox();
            fragment.Size = new Size(playerHeart.Width / 2, playerHeart.Height / 2);
            if (param == 1)
            {
                fragment.Image = Resources.yellowheartshard;
            }
            else
            {
                fragment.Image = Resources.heartshard;
            }
            fragment.SizeMode = PictureBoxSizeMode.StretchImage;
            fragment.Location = playerHeart.Location;
            fragment.BackColor = Color.Transparent;
            fragments.Add(fragment);
            velocities.Add(new PointF((float)(rnd.NextDouble() * 14 - 2), (float)(rnd.NextDouble() * -14 - 2)));
            positions.Add(new PointF(playerHeart.Location.X, playerHeart.Location.Y));
            playerHeart.Parent.Controls.Add(fragment);
        }

        timer.Interval = 30;
        timer.Tick += Timer_Tick;
        timer.Start();
    }

    private void Timer_Tick(object sender, EventArgs e)
    {
        timeStep++;
        if (timeStep > 100)
        {
            timer.Stop();
            foreach (var fragment in fragments)
            {
                fragment.Dispose();
            }
            return;
        }

        for (int i = 0; i < fragments.Count; i++)
        {
            velocities[i] = new PointF(velocities[i].X, velocities[i].Y + gravity);
            positions[i] = new PointF(positions[i].X + velocities[i].X, positions[i].Y + velocities[i].Y);
            fragments[i].Location = new Point((int)positions[i].X, (int)positions[i].Y);
        }
    }

    private void InitializeLabels()
    {
        gameOverLabel = new Label();
        restartLabel = new Label();

        gameOverLabel.Text = "GAME OVER";
        gameOverLabel.Font = new Font("Determination Extended", 80, FontStyle.Bold);
        gameOverLabel.ForeColor = Color.Yellow;
        gameOverLabel.BackColor = Color.Transparent;
        gameOverLabel.AutoSize = true;

        restartLabel.Text = "Натисни ESC";
        restartLabel.Font = new Font("Determination Extended", 18, FontStyle.Regular);
        restartLabel.ForeColor = Color.White;
        restartLabel.BackColor = Color.Transparent;
        restartLabel.AutoSize = true;

        playerHeart.Parent.Controls.Add(gameOverLabel);
        playerHeart.Parent.Controls.Add(restartLabel);

        gameOverLabel.Location = new Point(
            playerHeart.Parent.ClientSize.Width / 2 - gameOverLabel.PreferredSize.Width / 2,
            playerHeart.Parent.ClientSize.Height / 2 - gameOverLabel.PreferredSize.Height / 2);

        restartLabel.Location = new Point(
            playerHeart.Parent.ClientSize.Width / 2 - restartLabel.PreferredSize.Width / 2,
            gameOverLabel.Location.Y + gameOverLabel.PreferredSize.Height + 10);
    }

    public void heartBrokeAnim()
    {
        playerHeart.Visible = false;
    }
}
