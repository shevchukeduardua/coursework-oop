﻿namespace CourseWork
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.DifficultBox = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonHard = new System.Windows.Forms.RadioButton();
            this.buttonNormal = new System.Windows.Forms.RadioButton();
            this.buttonEasy = new System.Windows.Forms.RadioButton();
            this.boxInstaDeath = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelExit = new System.Windows.Forms.Label();
            this.DifficultBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // DifficultBox
            // 
            this.DifficultBox.Controls.Add(this.label2);
            this.DifficultBox.Controls.Add(this.label1);
            this.DifficultBox.Controls.Add(this.buttonHard);
            this.DifficultBox.Controls.Add(this.buttonNormal);
            this.DifficultBox.Controls.Add(this.buttonEasy);
            this.DifficultBox.Controls.Add(this.boxInstaDeath);
            this.DifficultBox.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DifficultBox.Location = new System.Drawing.Point(376, 14);
            this.DifficultBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DifficultBox.Name = "DifficultBox";
            this.DifficultBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DifficultBox.Size = new System.Drawing.Size(314, 459);
            this.DifficultBox.TabIndex = 0;
            this.DifficultBox.TabStop = false;
            this.DifficultBox.Text = "Складність";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Determination Extended", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(73, 191);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 34);
            this.label2.TabIndex = 5;
            this.label2.Text = "Керування: Стрілочки\r\nВибір: Enter\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 253);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 196);
            this.label1.TabIndex = 4;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonHard
            // 
            this.buttonHard.AutoSize = true;
            this.buttonHard.Location = new System.Drawing.Point(107, 91);
            this.buttonHard.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonHard.Name = "buttonHard";
            this.buttonHard.Size = new System.Drawing.Size(84, 18);
            this.buttonHard.TabIndex = 3;
            this.buttonHard.Text = "Складно";
            this.buttonHard.UseVisualStyleBackColor = true;
            this.buttonHard.CheckedChanged += new System.EventHandler(this.buttonHard_CheckedChanged);
            // 
            // buttonNormal
            // 
            this.buttonNormal.AutoSize = true;
            this.buttonNormal.Checked = true;
            this.buttonNormal.Location = new System.Drawing.Point(107, 66);
            this.buttonNormal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonNormal.Name = "buttonNormal";
            this.buttonNormal.Size = new System.Drawing.Size(99, 18);
            this.buttonNormal.TabIndex = 2;
            this.buttonNormal.TabStop = true;
            this.buttonNormal.Text = "Нормально";
            this.buttonNormal.UseVisualStyleBackColor = true;
            this.buttonNormal.CheckedChanged += new System.EventHandler(this.buttonNormal_CheckedChanged);
            // 
            // buttonEasy
            // 
            this.buttonEasy.AutoSize = true;
            this.buttonEasy.Location = new System.Drawing.Point(107, 41);
            this.buttonEasy.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonEasy.Name = "buttonEasy";
            this.buttonEasy.Size = new System.Drawing.Size(66, 18);
            this.buttonEasy.TabIndex = 1;
            this.buttonEasy.Text = "Легко";
            this.buttonEasy.UseVisualStyleBackColor = true;
            this.buttonEasy.CheckedChanged += new System.EventHandler(this.buttonEasy_CheckedChanged);
            // 
            // boxInstaDeath
            // 
            this.boxInstaDeath.AutoSize = true;
            this.boxInstaDeath.Location = new System.Drawing.Point(71, 142);
            this.boxInstaDeath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.boxInstaDeath.Name = "boxInstaDeath";
            this.boxInstaDeath.Size = new System.Drawing.Size(169, 18);
            this.boxInstaDeath.TabIndex = 0;
            this.boxInstaDeath.Text = "Моментальна смерть";
            this.boxInstaDeath.UseVisualStyleBackColor = true;
            this.boxInstaDeath.CheckedChanged += new System.EventHandler(this.boxInstaDeath_CheckedChanged);
            // 
            // startButton
            // 
            this.startButton.AutoSize = true;
            this.startButton.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.startButton.Font = new System.Drawing.Font("Determination Extended", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.startButton.Location = new System.Drawing.Point(123, 156);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(129, 35);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Почати";
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            this.startButton.MouseLeave += new System.EventHandler(this.startButton_MouseLeave);
            this.startButton.MouseHover += new System.EventHandler(this.startButton_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::CourseWork.Properties.Resources.yheart;
            this.pictureBox1.Location = new System.Drawing.Point(93, 156);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // labelExit
            // 
            this.labelExit.AutoSize = true;
            this.labelExit.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelExit.Font = new System.Drawing.Font("Determination Extended", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelExit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelExit.Location = new System.Drawing.Point(129, 267);
            this.labelExit.Name = "labelExit";
            this.labelExit.Size = new System.Drawing.Size(110, 35);
            this.labelExit.TabIndex = 3;
            this.labelExit.Text = "Вийти";
            this.labelExit.Click += new System.EventHandler(this.labelExit_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(708, 485);
            this.Controls.Add(this.labelExit);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.DifficultBox);
            this.Font = new System.Drawing.Font("Determination Extended", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MenuForm";
            this.Text = "Меню";
            this.DifficultBox.ResumeLayout(false);
            this.DifficultBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox DifficultBox;
        private System.Windows.Forms.RadioButton buttonEasy;
        private System.Windows.Forms.CheckBox boxInstaDeath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton buttonHard;
        private System.Windows.Forms.RadioButton buttonNormal;
        private System.Windows.Forms.Label startButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelExit;
    }
}