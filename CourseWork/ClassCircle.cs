﻿using System.Drawing;


    public class CircleHitbox
    {
        public Point Center { get; set; }
        public int Radius { get; set; }

        public CircleHitbox(Point center, int radius)
        {
            Center = center;
            Radius = radius;
        }

        public bool Intersects(CircleHitbox other)
        {
            if (other == null) return false;

            int dx = Center.X - other.Center.X;
            int dy = Center.Y - other.Center.Y;
            int distance = (dx * dx) + (dy * dy);
            int radiusSum = Radius + other.Radius;
            return distance <= radiusSum * radiusSum;
        }

        public Rectangle GetBounds()
        {
            return new Rectangle(Center.X - Radius, Center.Y - Radius, Radius * 2, Radius * 2);
        }
        public void UpdatePosition(Point newCenter)
        {
            Center = newCenter;
        }
    }


