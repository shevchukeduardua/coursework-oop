﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CourseWork
{
    public class Bullet : PictureBox
    {
        public int Speed { get; set; } = 5;
        public CircleHitbox Hitbox { get; set; }

        public Bullet()
        {
            this.Size = new System.Drawing.Size(32, 16);
            this.Location = new Point();
            this.Hitbox = new CircleHitbox(new Point(this.Location.X + this.Size.Width / 2, this.Location.Y + this.Size.Height / 2), Math.Min(this.Size.Width, this.Size.Height) / 2);
        }

        public void UpdatePosition(Point newCenter)
        {
            this.Hitbox.UpdatePosition(newCenter);
            this.Location = new Point(newCenter.X - this.Size.Width / 2, newCenter.Y - this.Size.Height / 2);
        }
    }
}
