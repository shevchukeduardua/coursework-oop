﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Media;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Timers;
using static System.Windows.Forms.AxHost;
using static CourseWork.mainWindow;
using CourseWork.Properties;
using CourseWork;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Reflection;
using NAudio.Wave;
using System.Net;
using System.Globalization;
using System.Resources;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolBar;
using System.Text.RegularExpressions;

namespace CourseWork
{ 
    public partial class mainWindow : Form
    {
        Random random = new Random();
        public int menuSelect = 1;
        public int moveSpeed = 2;
        public bool isMovingUp, isMovingDown, isMovingLeft, isMovingRight;
        public int playerVelocityY, playerVelocityX;
        public int playerHP = 40;
        public int attackIteration = 0;
        public int attackCount = 0;
        public int readyCount = 0;
        public int calmCount = 0;
        public int canSpare = 0;
        public int brokePhase = 0;
        public int isGravitaion = 100;
        public int isEnemyDeath = 0;
        public double enemyShield = 2.5;
        public int enemyAttack = 5;
        private List<Bottle> objects = new List<Bottle>();
        public List<Bullet> bullets = new List<Bullet>();
        private List<PictureBox> pistols = new List<PictureBox>();
        private List<Tuple<Point, Point, float>> lines = new List<Tuple<Point, Point, float>>();
        private ThanosSnapEffect snapEffect1;
        private ThanosSnapEffect snapEffect2;

        public double attackDelay { get; set; }
        public double playerDefence { get; set; }
        public double attackBaseSpeed { get; set; }
        public bool instaDeath { get; set; }

        private SoundPlayer simplePlayer = new SoundPlayer("text.wav");
        private IWavePlayer musicPlayer = new WaveOutEvent();
        private IWavePlayer soundPlayer = new WaveOutEvent();
        private IWavePlayer sound2Player = new WaveOutEvent();
        private IWavePlayer enviromentPlayer = new WaveOutEvent();
        private AudioFileReader musicTrack2 = new AudioFileReader("ghosttown1.wav");
        private AudioFileReader musicTrack1 = new AudioFileReader("showdown1.wav");
        private AudioFileReader musicTrack3 = new AudioFileReader("the_end_is_nigh.wav");
        private AudioFileReader playerHitSound = new AudioFileReader("damage-taken.wav");
        private AudioFileReader enemyHitSound = new AudioFileReader("shotSound.wav");
        private AudioFileReader bottleBrokeSound = new AudioFileReader("glassbreak.wav");
        private AudioFileReader buttonSound = new AudioFileReader("select.wav");
        private AudioFileReader transformSound = new AudioFileReader("transform.wav");
        private AudioFileReader gunSound = new AudioFileReader("gun_appered.wav");
        private AudioFileReader selectSound = new AudioFileReader("select_menu.wav");
        private AudioFileReader itemSound = new AudioFileReader("item_consume.wav");
        private AudioFileReader heartbroke = new AudioFileReader("heartBroke.wav");
        private AudioFileReader heartshard = new AudioFileReader("heartBroke2.wav");
        private AudioFileReader justice = new AudioFileReader("justice.wav");
        private AudioFileReader deathEnemy = new AudioFileReader("dust.wav");
        private int currentPlayingFile;

        public Bitmap originalImage;
        public Bitmap waterEffectImage;
        public int animationCounter = 0;

        int posCount;
        public Image[] shootFrames;
        public Image[] animationFrames;
        public Image[] animationPhase2Frames;
        public int currentFrameIndex = 0; public int shootFrameIndex = 4;
        public int isAnimationPlaying = 0;

        public int GamePhase = 0;
        public int textCount = 0;
        public string targetText = "";

        public PictureBox attackBox = new PictureBox();
        public PictureBox attackIndicator = new PictureBox();
        public PictureBox bulletAttack = new PictureBox();

        private CircleHitbox playerHitbox;
        int temp = 0;

        public mainWindow()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);

            animationFrames = new Image[]
            {
                Properties.Resources.enemy_anim1,
                Properties.Resources.enemy_anim2,
                Properties.Resources.enemy_anim3,
                Properties.Resources.enemy_anim4,
                Properties.Resources.enemy_anim5,
                Properties.Resources.enemy_anim6,
                Properties.Resources.enemy_anim7
            };
            animationPhase2Frames = new Image[]
            {
                Properties.Resources.enemy_2anim1,
                Properties.Resources.enemy_2anim2,
                Properties.Resources.enemy_2anim3,
                Properties.Resources.enemy_2anim4,
                Properties.Resources.enemy_2anim5,
                Properties.Resources.enemy_2anim6,
            };
            shootFrames = new Image[]
            {
                Properties.Resources.shoot1,
                Properties.Resources.shoot1,
                Properties.Resources.shoot1,
                Properties.Resources.shoot2,
                Properties.Resources.shoot2,
                Properties.Resources.shoot2,
                Properties.Resources.shoot3,
                Properties.Resources.shoot3,
                Properties.Resources.shoot3,
                Properties.Resources.shoot4,
                Properties.Resources.shoot4,
                Properties.Resources.shoot4,
                Properties.Resources.shoot5,
            };

            KeyDown += FormKeyDown;
            KeyUp += FormKeyUp;

            timer1.Interval = 1;
            timer1.Tick += GameLogicTimer_Tick;
            gameAnimationTimer.Tick += gameAnimationTimer_Tick;

            musicPlayer.PlaybackStopped += MusicPlayer_PlaybackStopped;
            musicPlayer.Init(musicTrack1);
            musicPlayer.Play();
            currentPlayingFile = 1;

            enemyHead.BringToFront();
            enemyHpBar.BringToFront();
            originalImage = new Bitmap(Properties.Resources.sunset);
            waterEffectImage = new Bitmap(originalImage.Width, originalImage.Height);
            
            attackBox.Location = new Point(125, 459);
            attackBox.SizeMode = PictureBoxSizeMode.StretchImage;
            attackBox.Size = new Size(0, 300);
            attackBox.Image = Properties.Resources.attack_bar;
            this.Controls.Add(attackBox);
            attackBox.SendToBack();

            attackIndicator.Location = new Point(0, 459);
            attackIndicator.SizeMode = PictureBoxSizeMode.StretchImage;
            attackIndicator.Size = new Size(20, 300);
            attackIndicator.BackColor = Color.White;
            this.Controls.Add(attackIndicator);
            attackIndicator.BringToFront();
            attackIndicator.Visible = false;

            playerHitbox = new CircleHitbox(new Point(playerHeart.Left + 12, playerHeart.Top + 12), 12);
            simplePlayer.Load();
        }

        private void ClearAllAttack()
        {
            foreach (var attackObject in pistols)
            {
                this.Controls.Remove(attackObject);
                attackObject.Dispose();
            }
            pistols.Clear();

            foreach (var line in lines)
            {
                ClearLine(line.Item1, line.Item2, line.Item3);
            }
            lines.Clear();

            foreach (Bottle obj in objects)
            {
                this.Controls.Remove(obj.PictureBox);
            }
            foreach (Bullet b in bullets)
            {
                b.Location = new Point(0, 0);
                this.Controls.Remove(b);
            }
            bullets.Clear();
            objects.Clear();
            bottleAttackTimer.Stop();
            bulletAttackTimer.Stop();
            shootAttackTimer.Stop();
        }

        private void AddObject(Point location, PointF velocity, bool isLarge, bool isRight)
        {
            PictureBox obj = new PictureBox
            {
                Size = isLarge ? new Size(16, 50) : new Size(12, 12),
                Location = location,
                Image = isLarge ? Properties.Resources.bottle : Properties.Resources.bottle_piece,
                SizeMode = PictureBoxSizeMode.StretchImage
            };
            this.Controls.Add(obj);
            obj.BringToFront();
            objects.Add(new Bottle(obj, velocity, isLarge, isRight));
        }

        private void SplitObject(Bottle movingObject, int index)
        {
            this.Controls.Remove(movingObject.PictureBox);
            objects.RemoveAt(index);

            AddObject(movingObject.PictureBox.Location, new PointF(-2, -18), false, false);
            AddObject(movingObject.PictureBox.Location, new PointF(2, -18), false, true);
            if (attackIteration <= 3)
            {
                AddObject(movingObject.PictureBox.Location, new PointF(2, -9), false, true);
                AddObject(movingObject.PictureBox.Location, new PointF(-2, -9), false, false);
            }
            if (attackIteration > 3)
            {
                AddObject(movingObject.PictureBox.Location, new PointF(0, -20), false, true);
                AddObject(movingObject.PictureBox.Location, new PointF(0, -20), false, false);
            }
        }

        private Bitmap BitImage(Image image)
        {
            Bitmap bitiamge = new Bitmap(image);
            return bitiamge;
        }

        public void ApplyWaterEffect()
        {
            BitmapData originalData = originalImage.LockBits(new Rectangle(0, 0, originalImage.Width, originalImage.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            BitmapData waterData = waterEffectImage.LockBits(new Rectangle(0, 0, waterEffectImage.Width, waterEffectImage.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            unsafe
            {
                byte* originalPtr = (byte*)originalData.Scan0;
                byte* waterPtr = (byte*)waterData.Scan0;

                double transparencyFactor = 0.7 + 0.3 * Math.Sin(animationCounter / 40.0);

                Parallel.For(0, originalData.Height, y =>
                {
                    int offsetY = y + animationCounter;

                    for (int x = 0; x < originalData.Width; x++)
                    {
                        int offsetX = x + (int)(Math.Sin(offsetY / 10.0) * 5);

                        if (offsetX >= 0 && offsetX < originalData.Width)
                        {
                            int originalIndex = (y * originalData.Stride) + (x * 4);
                            int waterIndex = (y * waterData.Stride) + (offsetX * 4);

                            for (int i = 0; i < 3; i++) 
                            {
                                waterPtr[waterIndex + i] = originalPtr[originalIndex + i];
                            }
                            waterPtr[waterIndex + 3] = (byte)(originalPtr[originalIndex + 3] * transparencyFactor);
                        }
                    }
                });
            }

            originalImage.UnlockBits(originalData);
            waterEffectImage.UnlockBits(waterData);
            enemyBody.BackgroundImage = waterEffectImage;
        }

        private void MusicPlayer_PlaybackStopped(object sender, StoppedEventArgs e)
        {

            if (currentPlayingFile != -1 && !cutsceneTimer.Enabled && canSpare != 2 && GamePhase != 10 && GamePhase != 11 && GamePhase != 3 && GamePhase != 4 && GamePhase != 5 && currentPlayingFile != -1)
            {
                musicTrack1.Position = 0;
                musicTrack2.Position = 0;
                musicTrack3.Position = 0;
                if (currentPlayingFile == 3)
                {
                    if (musicPlayer.PlaybackState == PlaybackState.Stopped)
                    {
                        musicPlayer.Init(musicTrack3);
                    }
                }
                else if (attackIteration > 3 && currentPlayingFile != 3)
                {
                    if (musicPlayer.PlaybackState == PlaybackState.Stopped)
                    {
                        musicPlayer.Init(musicTrack2);
                    }
                }
                else if (currentPlayingFile == 4)
                {
                    if (musicPlayer.PlaybackState == PlaybackState.Stopped)
                    {
                        musicPlayer.Init(justice);
                    }
                }
                else if (currentPlayingFile == 1)
                {
                    musicPlayer.Init(musicTrack1);
                }
                musicPlayer.Play();
            }
        }

        public void PlaySound(int soundID)
        {
            soundPlayer.Stop();
            buttonSound.Position = 0;
            enemyHitSound.Position = 0;
            gunSound.Position = 0;
            selectSound.Position = 0;
            itemSound.Position = 0;
            if (soundID == 0)
            {
                soundPlayer.Init(buttonSound);
                soundPlayer.Play();
            }
            else if (soundID == 1)
            {
                soundPlayer.Init(enemyHitSound);
                soundPlayer.Play();
            }
            else if (soundID == 2)
            {
                playerHitSound.Position = 0;
                sound2Player.Stop();
                sound2Player.Init(playerHitSound);
                sound2Player.Play();
            }
            else if (soundID == 3)
            {
                soundPlayer.Init(gunSound);
                soundPlayer.Play();
            }
            else if (soundID == 5)
            {
                soundPlayer.Init(selectSound);
                soundPlayer.Play();
            }
            else if (soundID == 4)
            {
                simplePlayer = new SoundPlayer("shot.wav");
                simplePlayer.Play();
            }
            else if (soundID == 6)
            {
                soundPlayer.Init(itemSound);
                soundPlayer.Play();
            }
        }

        public void BodyAnim_Tick(object sender, EventArgs e)
        {
            isAnimationPlaying = 1;
        }

        public void hitAminTimer_Tick(object sender, EventArgs e)
        {
            if (damageLabel.Text != Resources.attackMiss)
            {
                attackIteration++;
            }
            
            if (attackIteration == 4)
            {
                targetText = Resources.boxJustice;
                textBox.ForeColor = Color.Yellow;
                GamePhase = 6;
            }
            else
            {
                GamePhase = 5;
            }
            
            hitAminTimer.Stop();
        }

        public void afterAttackTimer_Tick(object sender, EventArgs e)
        {
            if (canSpare == 1)
            {
                afterAttackTimer.Interval = 1999;
                GamePhase = 0;
                afterAttackTimer.Stop();
            }
            else if (GamePhase == 11)
            {
                GamePhase = 13;
                afterAttackTimer.Stop();
                soundPlayer.Init(deathEnemy);
                soundPlayer.Play();
                enemyHpBar.Visible = false;
                damageLabel.Visible = false;
                textCount = 0;
                textBox.Text = string.Empty;
                targetText = Resources.boxWinGeno;
                textCount++;
            }
            else if (GamePhase == 13)
            {
                GamePhase++;
                textBox.Text = Resources.boxWinGeno;
                afterAttackTimer.Stop();
            }
            else if (GamePhase == 7)
            {
                if(attackIteration <= 3)
                {
                    textBox.ForeColor = Color.White;
                }
                GamePhase = 5;
            }
            else if (canSpare == 2)
            {
                afterAttackTimer.Interval = 27000;
                currentPlayingFile = 3;
                musicPlayer.Init(musicTrack3);
                musicPlayer.Play();
                soundPlayer.Init(transformSound);
                soundPlayer.Play();
                playerHeart.Image = Properties.Resources.yheart;
                playerHP = 40;
                attackCount = 0;
                canSpare = 1;
                GamePhase = 1;
            }
            else if (GamePhase != 1)
            {
                if (enemyHpBar.Value < 500 && enemyHpBar.Value > 0)
                {
                    if (currentPlayingFile == 2 || currentPlayingFile == 1)
                    {
                        afterAttackTimer.Interval = 12000;
                        currentPlayingFile = 3;
                        musicPlayer.Init(musicTrack3);
                        musicPlayer.Play();
                        soundPlayer.Init(transformSound);
                        soundPlayer.Play();
                        playerHeart.Image = Properties.Resources.yheart;
                        hpBar.Maximum = 60;
                        playerHP = 60;
                    }
                }
                else
                {
                    afterAttackTimer.Interval = 7000;
                }
                GamePhase = 1;
                afterAttackTimer.Stop();
            }
            else
            {
                GamePhase = 0;
                afterAttackTimer.Stop();
            }
            
        }

        public void playerHitTimer_Tick(object sender, EventArgs e)
        {
            if (currentPlayingFile != 3)
            {
                playerHeart.Image = Properties.Resources.heart;
            }
            else{
                playerHeart.Image = Properties.Resources.yheart;
            }
            
            playerHitTimer.Stop();
        }

        public void gameAnimationTimer_Tick(object sender, EventArgs e)
        {
            int startY = 21;
            int startX = 558;
            posCount++;

            if (GamePhase == 4)
            {
                if (shootFrameIndex != 13)
                {
                    shootFrame.Visible = true;
                    shootFrame.Image = shootFrames[shootFrameIndex];
                    shootFrameIndex++;
                }
                else
                {
                    shootFrame.Visible = false;
                }
            }

            if (GamePhase != 11 && GamePhase != 13)
            {
                if (attackIteration <= 3 && (currentPlayingFile != 3 || canSpare != 0))
                {
                    if (GamePhase != 4 || (!(damageLabel.Text == Resources.attackMiss) && GamePhase < 4))
                    {
                        enemyHead.Image = Properties.Resources.enemyhead;
                        int newYhead = startY + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                        enemyHead.Location = new Point(startX, newYhead);
                    }
                    else 
                    {
                        int newXhead = startX + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                        enemyHead.Location = new Point(newXhead, startY + 4);
                        enemyHead.Image = Properties.Resources.enemyhead_damage;
                    }
                }
                else if(attackIteration >= 3 && enemyHpBar.Value < 500 && canSpare == 0 && enemyHpBar.Value > 0) 
                {
                    enemyHead.Image = Properties.Resources.enemyhead_fall;
                    if (GamePhase != 4 || (!(damageLabel.Text == Resources.attackMiss) && GamePhase < 4))
                    {
                        int newYhead = startY + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                        enemyHead.Location = new Point(startX, newYhead);
                    }
                    else
                    {
                        int newXhead = startX + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                        enemyHead.Location = new Point(newXhead, startY + 4);
                        enemyHead.Image = Properties.Resources.enemyhead_damage;
                    }
                }
                else
                {
                    if (GamePhase != 4 || !(damageLabel.Text == Resources.attackMiss))
                    {
                        enemyHead.Image = Properties.Resources.enemy_head2;
                        int newYhead = startY + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                        enemyHead.Location = new Point(startX, newYhead);
                    }
                    else
                    {
                        int newXhead = startX + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                        enemyHead.Location = new Point(newXhead, startY + 4);
                        enemyHead.Image = Properties.Resources.enemy_head2;
                    }
                }
            }
            else
            {
                int newYhead = startY + 70 + (int)(Math.Sin(posCount * Math.PI / 90) * 4);
                enemyHead.Location = new Point(startX, newYhead);
            }

            if (GamePhase != 11 && GamePhase != 13)
            {
                if (attackIteration < 4)
                {
                    if (isAnimationPlaying == 1)
                    {
                        if (currentFrameIndex != 7)
                        {
                            enemyBody.Image = animationFrames[currentFrameIndex];
                            currentFrameIndex++;
                        }
                        else
                        {
                            currentFrameIndex = 0;
                            isAnimationPlaying = 0;
                        }
                    }
                    else
                    {
                        currentFrameIndex = 0;
                        enemyBody.Image = animationFrames[currentFrameIndex];
                    }
                }
                else
                {
                    if (isAnimationPlaying == 1)
                    {
                        if (currentFrameIndex != 6)
                        {
                            enemyBody.Image = animationPhase2Frames[currentFrameIndex];
                            currentFrameIndex++;
                        }
                        else
                        {
                            currentFrameIndex = 5;
                            enemyBody.Image = animationFrames[currentFrameIndex];
                        }
                    }
                }
            }
       
            if (GamePhase == 0 || GamePhase == 7 || GamePhase == 13)
            {
                if (textCount < targetText.Length)
                {
                    simplePlayer = new SoundPlayer("text.wav");
                    simplePlayer.Play();
                    textBox.Text += targetText[textCount];
                    textCount++;
                }
            }
            else
            {
                textBox.Text = "";
                textCount = 0;
            }
        }

        public void FormKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    isMovingUp = true;
                    break;
                case Keys.Down:
                    isMovingDown = true;
                    break;
                case Keys.Left:
                    isMovingLeft = true;
                    break;
                case Keys.Right:
                    isMovingRight = true;
                    break;
                case Keys.Enter:
                    if (GamePhase == 2)
                    {
                        PlaySound(1);
                        GamePhase = 3;
                        enemyHpBar.Visible = true;
                        shootFrameIndex = 0;
                    }
                    else if (GamePhase == 0)
                    {
                        PlaySound(0);
                        fightButton.Image = Properties.Resources.fight_button;
                        actButton.Image = Properties.Resources.act_button;
                        itemButton.Image = Properties.Resources.item_button;

                        if (menuSelect == 0)
                        {
                            textBox.Text = "";
                            textBox.Visible = false;
                            attackBox.Location = new Point(125, 459);
                            attackBox.Size = new Size(0, 300);
                            attackIndicator.Location = new Point(0, 459);

                            GamePhase = 2;

                            attackCount++;
                        }
                        else if(menuSelect == 1)
                        {
                            menuSelect = 0;
                            GamePhase = 8;
                        }
                        else if(menuSelect == 2)
                        {
                            textBox.ForeColor = Color.Yellow;
                            textCount = 0;
                            textBox.Text = string.Empty;
                            int addHP = random.Next(7, 21);
                            int maxHP = hpBar.Maximum;
                            if (playerHP + addHP > maxHP)
                            {
                                targetText = Resources.boxMaxHp;
                                playerHP = maxHP;
                            }
                            else
                            {
                                targetText = Resources.boxHpUp + addHP.ToString();
                                playerHP += addHP;
                            }
                            GamePhase = 7;
                            PlaySound(6);
                            attackCount++;
                        }
                    }
                    else if (GamePhase == 8)
                    {
                        PlaySound(0);
                        fightButton.Image = Properties.Resources.fight_button;
                        actButton.Image = Properties.Resources.act_button;
                        itemButton.Image = Properties.Resources.item_button;

                        if (menuSelect == 0)
                        {
                            textCount = 0;
                            textBox.Text = string.Empty;
                            if (calmCount >= 3 && readyCount >= 3 && canSpare == 0)
                            {
                                cutsceneTimer.Start();
                                musicPlayer.Stop();
                                targetText = Resources.messageFinalAttack;
                                canSpare = 2;
                                GamePhase = 7;
                            }
                            else if (canSpare == 0)
                            {
                                targetText = $"{Resources.messageDesc1}{enemyAttack} {Resources.messageDesc2}{enemyShield * 10}     {Resources.messageDesc3}";
                                GamePhase = 7;
                            }
                            else
                            {
                                GamePhase = 14;
                            }
                            
                            PlaySound(0);
                            attackCount++;
                        }
                        else if (menuSelect == 1)
                        {
                            textCount = 0;
                            textBox.Text = string.Empty;
                            if (!(attackIteration > 3))
                            {
                                calmCount++;
                                if (calmCount < 3)
                                {
                                    targetText = Resources.messageCalm;
                                }
                                else if (calmCount >= 3 && readyCount < 3)
                                {
                                    targetText = Resources.messagePreSpare1;
                                }
                                else
                                {
                                    targetText = Resources.messagePreSpare2;
                                }
                            }
                            else
                            {
                                targetText = Resources.messageGeno2;
                            }
                            GamePhase = 7;
                            PlaySound(0);
                            attackCount++;
                        }
                        else if (menuSelect == 2)
                        {
                            textCount = 0;
                            textBox.Text = string.Empty;
                            if (!(attackIteration > 3))
                            {
                                readyCount++;
                                if (readyCount < 3)
                                {
                                    targetText = Resources.messageReady;
                                    enemyAttack = 10;
                                }
                                else if (calmCount < 3 && readyCount >= 3)
                                {
                                    targetText = Resources.messagePreSpare1;
                                }
                                else
                                {
                                    targetText = Resources.messagePreSpare2;
                                }
                            }
                            else
                            {
                                targetText = Resources.messageGeno1;
                            }
                            GamePhase = 7;
                            PlaySound(0);
                            attackCount++;
                        }

                        labelSelect1.Visible = false;
                        labelSelect2.Visible = false;
                        labelSelect3.Visible = false;
                        labelSelect3.Visible = false;
                    }
                    break;
                case Keys.Escape:
                    musicPlayer.Dispose();
                    sound2Player.Stop();
                    soundPlayer.Stop();
                    this.Dispose();
                    this.Close();
                    break;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            musicPlayer.Stop();
        }

        public void bulletAttackTimer_Tick(object sender, EventArgs e)
        {
            int maxSpeed = (int)(7 * attackBaseSpeed) + attackIteration;
            PlaySound(4);
            Bullet bullet = new Bullet();
            this.Controls.Add(bulletAttack);
            if (attackCount % 2 == 0 || attackIteration > 3)
            {

                bullet.Speed = random.Next((int)(5 * attackBaseSpeed), maxSpeed);
                bullet.Location = new Point(100, playerHeart.Location.Y + 11);
                bullet.Image = Properties.Resources.bullet;
                this.Controls.Add(bullet);
                bullets.Add(bullet);
            }

            if (attackIteration > 3 || attackCount % 2 == 1)
            {
                Bullet BackBullet = new Bullet();
                this.Controls.Add(bulletAttack);
                BackBullet.Speed = random.Next(-(maxSpeed), (int)(-5 * attackBaseSpeed));
                BackBullet.Location = new Point(1180, playerHeart.Location.Y - 6);
                BackBullet.Image = Properties.Resources.bulletback;
                this.Controls.Add(BackBullet);
                bullets.Add(BackBullet);
            }
        }

        public void cutsceneTimer_Tick(object sender, EventArgs e)
        {
            if (canSpare != 0)
            {
                cutsceneTimer.Stop();
            }
            else
            {
                musicPlayer?.Stop();
                musicPlayer.PlaybackStopped += MusicPlayer_PlaybackStopped;
                musicTrack2.Position = 0;
                musicPlayer.Init(musicTrack2);
                musicPlayer.Play();
                currentPlayingFile = 2;
                cutsceneTimer.Stop();
            }
        }

        public void FormKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    if (GamePhase == 8) 
                    {
                        menuSelect--;
                        PlaySound(5);
                    }
                    isMovingUp = false;
                    break;
                case Keys.Down:
                    if (GamePhase == 8)
                    {
                        menuSelect++;
                        PlaySound(5);
                    }
                    isMovingDown = false;
                    break;
                case Keys.Left:
                    if (GamePhase == 8 || GamePhase == 0)
                    {
                        menuSelect--;
                        PlaySound(5);
                    }
                    isMovingLeft = false;
                    break;
                case Keys.Right:
                    if (GamePhase == 8 || GamePhase == 0)
                    {
                        menuSelect++;
                        PlaySound(5);
                    }
                    isMovingRight = false;
                    break;
            }
        }

        void UpdatePlayerVelocity()
        {
            playerVelocityX = 0;
            playerVelocityY = 0;

            if (isMovingLeft)
            {
                playerVelocityX = -moveSpeed;
            }
            if (isMovingRight)
            {
                playerVelocityX = moveSpeed;
            }
            

            if (isGravitaion >= 66)
            {
                attackBoxBottom.BackColor = Color.Yellow;
                labelGravity.Visible = true;
                if (!isMovingDown && !isMovingUp)
                {
                    playerVelocityY = 1;
                }
                else if (isMovingUp) 
                {
                    playerVelocityY = -(moveSpeed - 1);
                }
                else if (isMovingDown) 
                {
                    playerVelocityY = moveSpeed + 1;
                }
            }
            else
            {
                if (isMovingUp)
                {
                    playerVelocityY = -moveSpeed;
                }
                if (isMovingDown)
                {
                    playerVelocityY = moveSpeed;
                }
            }
        }

        void UpdatePlayerPosition()
        {
            playerHeart.Left += playerVelocityX;
            playerHeart.Top += playerVelocityY;
            playerHitbox.UpdatePosition(new Point(playerHeart.Left + 12, playerHeart.Top + 12));
        }

        private void bottleAttackTimer_Tick(object sender, EventArgs e)
        {

            if (temp % 2 == 0)
            {
                AddObject(new Point(0, this.Height / 2), new PointF(5, -10), true, true); temp++;
            }
            else
            {
                AddObject(new Point(this.Width - 50, this.Height / 2), new PointF(-5, -10), true, false); temp++;
            }

            if (attackIteration > 3)
            {
                temp = random.Next(0, 2);
            }
        }

        public void AttackPlayer(PictureBox attackObject, Point playerPosition)
        {
            if (GamePhase == 1)
            {
                bool fromLeft = random.Next(2) == 0;
                int startX = fromLeft ? 250 : this.ClientSize.Width - 250;
                int startY = random.Next(450, 750); 

                Point startPoint = new Point(startX, startY);
                Point endPoint = new Point(fromLeft ? this.ClientSize.Width : 0, startPoint.Y);

                PictureBox attackObjectPistol = new PictureBox();
                attackObjectPistol.Size = new Size(46, 29);
                attackObjectPistol.Image = fromLeft ? Properties.Resources.pistol : Properties.Resources.pistolR;
                attackObjectPistol.Location = startPoint;
                this.Controls.Add(attackObjectPistol);
                pistols.Add(attackObjectPistol);

                startPoint = new Point(startX, startY + 4);

                PlaySound(3);

                System.Windows.Forms.Timer semiTransparentTimer = new System.Windows.Forms.Timer();
                semiTransparentTimer.Interval = (int)(50 * attackBaseSpeed); 
                semiTransparentTimer.Tick += (s, e) =>
                {
                    DrawLine(startPoint, endPoint, Color.FromArgb(128, 255, 255, 255), 4); 
                    semiTransparentTimer.Stop();
                    semiTransparentTimer.Dispose();

                    System.Windows.Forms.Timer fullWhiteTimer = new System.Windows.Forms.Timer();
                    fullWhiteTimer.Interval = 500; 
                    fullWhiteTimer.Tick += (s2, e2) =>
                    {
                        DrawLine(startPoint, endPoint, Color.White, 4); 
                        lines.Add(Tuple.Create(startPoint, endPoint, 4f));
                        fullWhiteTimer.Stop();
                        fullWhiteTimer.Dispose();

                        Rectangle playerHitbox = new Rectangle(playerHeart.Location, playerHeart.Size);
                        if (IsLineIntersectingRectangle(startPoint, endPoint, playerHitbox))
                        {
                            PlayerDamaged();
                        }

                        System.Windows.Forms.Timer removeObjectsTimer = new System.Windows.Forms.Timer();
                        removeObjectsTimer.Interval = 200;
                        removeObjectsTimer.Tick += (s3, e3) =>
                        {
                            ClearLine(startPoint, endPoint, 4);
                            this.Controls.Remove(attackObjectPistol);
                            pistols.Remove(attackObjectPistol);
                            attackObjectPistol.Dispose();
                            lines.Remove(Tuple.Create(startPoint, endPoint, 4f));
                            removeObjectsTimer.Stop();
                            removeObjectsTimer.Dispose();
                        };
                        removeObjectsTimer.Start();
                    };
                    fullWhiteTimer.Start();
                    PlaySound(4);
                };
                semiTransparentTimer.Start();
            }
        }

        private void ClearLine(Point start, Point end, float thickness)
        {
            using (Graphics g = this.CreateGraphics())
            using (Pen pen = new Pen(this.BackColor, thickness))
            {
                g.DrawLine(pen, start, end);
            }
        }

        private void DrawLine(Point start, Point end, Color color, float thickness)
        {
            if (GamePhase == 1)
            {
                using (Graphics g = this.CreateGraphics())
                using (Pen pen = new Pen(color, thickness))
                {
                    g.DrawLine(pen, start, end);
                }
            }
            
        }

        private bool IsLineIntersectingRectangle(Point lineStart, Point lineEnd, Rectangle rect)
        {
            if (lineStart.Y >= rect.Top && lineStart.Y <= rect.Bottom)
            {
                return true;
            }
            return false;
        }

        private void shootAttackTimer_Tick(object sender, EventArgs e)
        {
            Point playerPosition = playerHeart.Location;
            SpawnPistolAttack(playerPosition);
        }

        public void SpawnPistolAttack(Point playerPosition)
        {
            if (GamePhase == 1) 
            {
                PictureBox attackObject = new PictureBox();
                attackObject.Size = new Size(20, 20);
                attackObject.BackColor = Color.Black;
                attackObject.Location = new Point(200, 200);
                this.Controls.Add(attackObject);

                System.Windows.Forms.Timer attackTimer = new System.Windows.Forms.Timer();
                attackTimer.Interval = 1000;
                attackTimer.Tick += (s, e) =>
                {
                    attackTimer.Stop();
                    AttackPlayer(attackObject, playerPosition);
                    this.Controls.Remove(attackObject);
                };
                attackTimer.Start();
            }
            
        }

        void AttackSelect()
        {
            if(canSpare == 0)
            {
                if (attackIteration > 3 && isGravitaion == 0)
                {
                    isGravitaion = random.Next(0, 101);
                }
                switch (attackCount % 6)
                {
                    case 1:
                        if (currentPlayingFile == 3)
                        {
                            bulletAttackTimer.Interval = (int)(600 / attackDelay);
                        }
                        else
                        {
                            bulletAttackTimer.Interval = (int)(750 / attackDelay);
                        }
                        bulletAttackTimer.Start();
                        break;
                    case 2:
                        shootAttackTimer.Interval = (int)(Math.Max(300 / attackDelay, 600 / attackDelay - attackIteration * 30 * attackDelay));
                        shootAttackTimer.Start();
                        break;
                    case 3:
                        bottleAttackTimer.Interval = (int)(Math.Max(950 / attackDelay, 2000 / attackDelay - attackIteration * 200 * attackDelay));
                        bottleAttackTimer.Start();
                        break;
                    case 0:
                        bottleAttackTimer.Interval = (int)(Math.Max(1400 / attackDelay, 4000 / attackDelay - attackIteration * 200 * attackDelay));
                        bulletAttackTimer.Interval = (int)(1200 / attackDelay - Math.Max(300 / attackDelay, -attackIteration * 30 * attackDelay));
                        bulletAttackTimer.Start();
                        bottleAttackTimer.Start();
                        break;
                    case 4:
                        shootAttackTimer.Interval = (int)(1000 / attackDelay - Math.Max(300 / attackDelay, -attackIteration * 30 * attackDelay));
                        shootAttackTimer.Start();
                        bottleAttackTimer.Interval = (int)(Math.Max(1200 / attackDelay, 3000 - attackIteration * attackDelay));
                        bottleAttackTimer.Start();
                        break;
                    case 5:
                        shootAttackTimer.Interval = (int)(1000 / attackDelay - Math.Max(450 / attackDelay, -attackIteration * 30 * attackDelay));
                        shootAttackTimer.Start();
                        isGravitaion = 100;
                        break;
                }
            }
            else if (canSpare == 1)
            {
                switch (attackCount % 6)
                {
                    case 1:
                        shootAttackTimer.Interval = 400;
                        shootAttackTimer.Start();
                        bottleAttackTimer.Stop();
                        break;
                    case 2:
                        bottleAttackTimer.Interval = 1300;
                        shootAttackTimer.Stop();
                        bottleAttackTimer.Start();
                        break;
                    case 0:
                        bottleAttackTimer.Interval = 1500;
                        bulletAttackTimer.Interval = 750;
                        bulletAttackTimer.Start();
                        bottleAttackTimer.Start();
                        break;
                    case 3:
                        shootAttackTimer.Interval = 700; 
                        shootAttackTimer.Start();
                        bulletAttackTimer.Stop();
                        break;
                    case 4:
                        shootAttackTimer.Interval = 420;
                        shootAttackTimer.Start();
                        bottleAttackTimer.Stop();
                        isGravitaion = 100;
                        break;
                }
                finalAttackTimer.Start();
            }

        }

        private void finalAttackTimer_Tick(object sender, EventArgs e)
        {
            attackCount++;
        }

        private void deathTimer_Tick(object sender, EventArgs e)
        {
            brokePhase++;
            if(brokePhase == 1)
            {
                soundPlayer.Init(heartbroke);
                soundPlayer.Play();

                if (playerHeart.Image == Properties.Resources.yheart)
                {
                    playerHeart.Image = Properties.Resources.yellowheartbroke;
                }
                else
                {
                    playerHeart.Image = Properties.Resources.heartbroke;
                }
            }
            else if (brokePhase == 2)
            {

                if (playerHeart.Image == Properties.Resources.yellowheartbroke)
                {
                    HeartAnimation anim = new HeartAnimation(playerHeart, 1);
                    anim.heartBrokeAnim();
                }
                else
                {
                    HeartAnimation anim = new HeartAnimation(playerHeart, 0);
                    anim.heartBrokeAnim();
                }
                soundPlayer.Init(heartshard);
                soundPlayer.Play();
                currentPlayingFile = -1;
            }
            else if (brokePhase == 3)
            {
                musicPlayer.Init(justice);
                musicPlayer.Play();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            currentPlayingFile = -1;
            musicPlayer.Stop();
            soundPlayer.Stop();
            sound2Player.Stop();
            bulletAttackTimer.Stop();
            bottleAttackTimer.Stop();
        }

        void PlayerDamaged()
        {
            if (!playerHitTimer.Enabled && GamePhase == 1)
            {
                PlaySound(2);
                if (instaDeath == true)
                {
                    playerHP = 0;
                    currentPlayingFile = -1;
                    GamePhase = 10;
                    musicPlayer.Stop();
                }
                else
                {
                    if (currentPlayingFile != 3)
                    {
                        playerHeart.Image = Properties.Resources.heart_damaged;
                    }
                    else
                    {
                        playerHeart.Image = Properties.Resources.yheart_damaged;
                    }

                    if (playerHP - (int)(enemyAttack / playerDefence) >= 0)
                    {
                        playerHP -= (int)(enemyAttack / playerDefence);
                    }
                    else
                    {
                        playerHP = 0;
                    }
                }
                playerHitTimer.Start();
            }
        }

        private bool CheckCollision(PictureBox bottle, Bullet bullet)
        {
            return bottle.Bounds.IntersectsWith(bullet.Bounds);
        }

        private void attackBoxMove()
        {
            int targetWidth = 1040;
            int targetX = 120;
            int stepWidth = 10;
            int stepX = 20;
            if (attackBoxBottom.Width < targetWidth)
            {
                attackBoxTop.Width += stepWidth * 3;
                attackBoxBottom.Width += stepWidth * 3;
                attackBoxRight.Location = new Point(1240 - attackBoxTop.Location.X + 30, attackBoxTop.Location.Y);
            }

            if (attackBoxBottom.Location.X > targetX)
            {
                attackBoxBottom.Location = new Point(attackBoxBottom.Location.X - stepX, attackBoxBottom.Location.Y);
                attackBoxTop.Location = new Point(attackBoxTop.Location.X - stepX, attackBoxTop.Location.Y);
                attackBoxLeft.Location = new Point(attackBoxTop.Location.X - stepX + 20, attackBoxTop.Location.Y);
            }
        }

        public void GameLogicTimer_Tick(object sender, EventArgs e)
        {
            enemyShield = 0.1;
            if (instaDeath == true)
            {
                hpBar.Visible = false;
                hpNameLabel.Visible = false;
            }

            Rectangle playerBound = playerHitbox.GetBounds();

            if (attackBoxBottom.Bounds.IntersectsWith(playerBound) && isGravitaion >= 66 && !playerHitTimer.Enabled)
            {
                PlaySound(2);
                playerHitTimer.Start();
                if (currentPlayingFile != 3)
                {
                    playerHeart.Image = Properties.Resources.heart_damaged;
                }
                else
                {
                    playerHeart.Image = Properties.Resources.yheart_damaged;
                }
                if (hpBar.Maximum > 0)
                {
                    hpBar.Maximum -= 2;
                }
            }

            for (int i = objects.Count - 1; i >= 0; i--)
            {
                int randomVal = 5 + random.Next(-5, 10);
                Bottle movingObject = objects[i];
                PictureBox obj = movingObject.PictureBox;
                PointF velocity = movingObject.Velocity;
                int velocityX = (int)velocity.X;
                int velocityY = (int)velocity.Y;
                Point objLocation = obj.Location;

                if (movingObject.IsRight)
                {
                    objLocation.X += velocityX + randomVal;
                }
                else
                {
                    objLocation.X += velocityX - randomVal;
                }
                objLocation.Y += velocityY;
                obj.Location = objLocation;

                obj.Image = BitImage(movingObject.OriginalImage);

                movingObject.Velocity = new PointF(velocity.X, velocity.Y + 0.5f);

                if (obj.Bounds.IntersectsWith(attackBoxBottom.Bounds))
                {
                    bottleBrokeSound.Position = 0;
                    bottleBrokeSound.Volume = 0.3F;
                    enviromentPlayer.Stop();
                    enviromentPlayer.Init(bottleBrokeSound);
                    enviromentPlayer.Play();

                    SplitObject(movingObject, i);
                    objects.Remove(movingObject);
                }

                if (playerBound.IntersectsWith(obj.Bounds))
                {
                    PlayerDamaged();
                }

                if (!movingObject.IsLarge && objLocation.Y < 0)
                {
                    this.Controls.Remove(obj);
                    objects.RemoveAt(i);
                }
            }

            animationCounter++; 
            ApplyWaterEffect();

            if (currentPlayingFile != 3 || canSpare != 0)
            {
                hpNameLabel.Text = "HP " + playerHP.ToString("") + "/" + hpBar.Maximum.ToString();
            }
            else
            {
                hpNameLabel.Text = "HP " + playerHP.ToString("") + "/" + hpBar.Maximum.ToString();
            }

            if (playerHP > hpBar.Maximum)
            {
                playerHP = hpBar.Maximum;
            }
            hpBar.Value = playerHP;
            
            foreach (Bullet b in bullets)
            {
                Point newLocation = new Point(b.Location.X + b.Speed, b.Location.Y);
                b.Location = newLocation;
                if (b.Location.X >= 1200 && b.Speed > 0)
                {
                    b.Dispose();
                }
                else if (b.Location.X <= 80 && b.Speed < 0)
                {
                    b.Dispose();
                }
                if (playerBound.IntersectsWith(b.Bounds))
                {
                    b.Dispose();
                    PlayerDamaged();
                }
            }

            switch (GamePhase)
            {
                case 0:
                    if (playerHP <= 0 && (GamePhase != 12 || GamePhase != 10))
                    {
                        GamePhase = 10;
                        currentPlayingFile = -1;
                    }

                    if (canSpare == 1 && musicPlayer.Volume > 0.1 && currentPlayingFile != -1)
                    {
                        musicPlayer.Volume -= 0.01F;
                    }
                    else if (canSpare == 1 && musicPlayer.Volume <= 0.1)
                    {
                        musicPlayer.Stop();
                        currentPlayingFile = -1;
                    }
                    else
                    {
                        musicPlayer.Volume = 1;
                    }

                    if (currentPlayingFile == 3)
                    {
                        enemyAttack = 9;
                    } 
                    else if (attackIteration >= 3)
                    {
                        enemyAttack = 7;
                    }
                    else
                    {
                        enemyAttack = 5;
                    }

                    attackBoxBottom.BackColor = Color.White;
                    labelGravity.Visible = false; 
                    isGravitaion = 0;
                    if (canSpare == 1)
                    {
                        targetText = Resources.boxCanSpare;
                    }
                    else if (currentPlayingFile == 3)
                    {
                        targetText = Resources.boxJustice;
                    }
                    else if (attackIteration == 4)
                    {
                        targetText = Resources.boxGenoMessage1;
                    }
                    else if (attackIteration < 7 && attackIteration > 4)
                    {
                        targetText = Resources.boxGenoMessage2;
                    }
                    else if (attackIteration >= 7)
                    {
                        targetText = Resources.boxEndIsNigh;
                    }
                    else if (attackIteration > 2)
                    {
                        targetText = Resources.boxPreGeno;
                    }
                    else if (readyCount >= 3 && calmCount >= 3)
                    {
                        enemyShield = 0.1;
                        targetText = Resources.boxPreSpare;
                    }
                    else if (readyCount > 2 || calmCount > 2)
                    {
                        targetText = Resources.boxShy;
                    }
                    else if (readyCount > 0 || calmCount > 0)
                    {
                        targetText = Properties.Resources.boxDefualt;
                    }
                    else if (playerHP < 20 && attackIteration <= 3)
                    {
                        targetText = Properties.Resources.boxLowHP;
                    }
                    else if (attackIteration <= 3)
                    {
                        targetText = Properties.Resources.boxEncount;
                    } 

                    if (menuSelect == 0)
                    {
                        fightButton.Image = Properties.Resources.fight_button_cheked;
                        actButton.Image = Properties.Resources.act_button;
                        itemButton.Image = Properties.Resources.item_button;
                    }
                    else if (menuSelect == 1)
                    {
                        fightButton.Image = Properties.Resources.fight_button;
                        actButton.Image = Properties.Resources.act_button_checked;
                        itemButton.Image = Properties.Resources.item_button;
                    }
                    else if (menuSelect == 2)
                    {
                        fightButton.Image = Properties.Resources.fight_button;
                        actButton.Image = Properties.Resources.act_button;
                        itemButton.Image = Properties.Resources.item_button_checked;
                    }
                    else if (menuSelect < 0)
                    {
                        menuSelect = 2;
                    }else
                    {
                        menuSelect = 0;
                    }

                    ClearAllAttack();

                    playerHeart.Visible = false;
                    textBox.Visible = true;
                    attackBoxMove();
                    break; 
                case 1:
                    if (playerHP <= 0 && (GamePhase != 12 || GamePhase != 10))
                    {
                        GamePhase = 10;
                    }
                    if (canSpare == 0 && currentPlayingFile == 3)
                    {
                        afterAttackTimer.Interval = 12000;
                    }
                    afterAttackTimer.Start();

                    AttackSelect();
                    fightButton.Image = Properties.Resources.fight_button;
                    actButton.Image = Properties.Resources.act_button;
                    itemButton.Image = Properties.Resources.item_button;
                    attackBox.Visible = false;
                    enemyHpBar.Visible = false;

                    UpdatePlayerVelocity();
                    UpdatePlayerPosition();

                    int newLeft = playerHeart.Left + playerVelocityX;
                    int newTop = playerHeart.Top + playerVelocityY;
                    if (newLeft < 495)
                    {
                        newLeft = 495;
                    }
                    else if (newLeft + playerHeart.Width > 790)
                    {
                        newLeft = 790 - playerHeart.Width;
                    }
                    if (newTop < 464)
                    {
                        newTop = 464;
                    }
                    else if (newTop + playerHeart.Height > 754)
                    {
                        newTop = 754 - playerHeart.Height;
                    }
                    playerHeart.Left = newLeft;
                    playerHeart.Top = newTop;
                    break;
                case 2:
                    attackIndicator.Visible = true;
                    attackBox.Visible = true;
                    int attakcBoxWidth = 1020;
                    if (attackBox.Width < attakcBoxWidth)
                    {
                        attackBox.Width += 60;
                    }

                    if (attackIndicator.Location.X < 1200)
                    {
                        attackIndicator.Location = new Point(attackIndicator.Location.X + 15, attackIndicator.Location.Y);
                    }

                    if (attackIndicator.Location.X >= 1200) 
                    {
                        GamePhase = 3;
                    }
                    attackBoxMove();
                    break;
                case 3:
                    int damage = 0;
                    if (attackIndicator.Location.X <= 640)
                    {
                        damageLabel.Visible = true;
                        damage = (int)(attackIndicator.Location.X / enemyShield);
                    }
                    else if (attackIndicator.Location.X > 640)
                    {
                        damageLabel.Visible = true;
                        damage = (int)((1280 - attackIndicator.Location.X) / enemyShield);
                    }
                    if (enemyHpBar.Value - damage > 0)
                    {
                        damageLabel.Text = damage.ToString();
                        if (damage <= 100 / enemyShield)
                        {
                            damage = 0;
                            damageLabel.Text = Resources.attackMiss;
                        }
                        enemyHpBar.Value -= damage;
                        GamePhase = 4;
                    }
                    else
                    {
                        damageLabel.Text = damage.ToString();
                        enemyHead.Image = Properties.Resources.enemyhead_fall;
                        enemyBody.Image = Properties.Resources.enemy_fall;
                        musicPlayer.Stop();
                        enemyHpBar.Value = 0;
                        GamePhase = 5;
                    }
                    if (enemyHpBar.Value < 500 && currentPlayingFile != 3 && enemyHpBar.Value > 0)
                    {
                        enemyAttack = 9;
                        musicPlayer?.Stop();
                    }
                    attackBoxMove();    
                    break;
                case 4:
                    hitAminTimer.Enabled = true;
                    if (attackIndicator.Visible == Visible)
                    {
                        attackIndicator.Visible = false;
                    }
                    else
                    {
                        attackIndicator.Visible = true;
                    }

                    break;
                case 5:
                    attackIndicator.Visible = false;

                    if (attackBox.Width > 0)
                    {
                        attackBox.Width -= 60;
                    }

                    if (enemyHpBar.Value > 0)
                    {
                        enemyHpBar.Visible = false;
                        playerHeart.Visible = true;
                        damageLabel.Visible = false;
                        playerHeart.Location = new Point(627, 595);

                        afterAttackTimer.Interval = 2000;
                        afterAttackTimer.Start();

                        if (attackBoxBottom.Width > 300)
                        {
                            attackBoxTop.Width -= 10 * 3;
                            attackBoxBottom.Width -= 10 * 3;
                            attackBoxRight.Location = new Point(1240 - attackBoxTop.Location.X + 25, attackBoxTop.Location.Y);
                        }

                        if (attackBoxBottom.Width <= 300)
                        {
                            attackBoxRight.Location = new Point(790, attackBoxTop.Location.Y);
                        }

                        if (attackBoxBottom.Location.X < 490)
                        {
                            attackBoxBottom.Location = new Point(attackBoxBottom.Location.X + 20, attackBoxBottom.Location.Y);
                            attackBoxTop.Location = new Point(attackBoxTop.Location.X + 20, attackBoxTop.Location.Y);
                            attackBoxLeft.Location = new Point(attackBoxTop.Location.X - 20 + 20, attackBoxTop.Location.Y);
                        }

                        if (attackBoxBottom.Width < 300)
                        {
                            attackBoxTop.Width = 300;
                            attackBoxBottom.Width = 300;
                        }
                    }

                    else
                    {
                        canSpare = 0;
                        enemyHead.Image = Resources.enemyhead_fall;
                        isEnemyDeath = 1;
                        GamePhase = 11;
                    }
                    
                    break;
                case 6:
                    cutsceneTimer.Start();
                    musicPlayer.Stop();

                    enemyAttack = 7;
                    enemyHead.Image = Properties.Resources.enemy_head2;

                    attackIndicator.Location = new Point(0, attackIndicator.Location.Y);
                    attackIndicator.Visible = false;
                    attackBox.Visible = false;
                    enemyHpBar.Visible = false;
                    playerHeart.Visible = true;
                    damageLabel.Visible = false;
                    playerHeart.Location = new Point(627, 595);

                    afterAttackTimer.Interval = 2000;
                    afterAttackTimer.Start();

                    if (attackBoxBottom.Width > 300)
                    {
                        attackBoxTop.Width -= 10 * 3;
                        attackBoxBottom.Width -= 10 * 3;
                        attackBoxRight.Location = new Point(1240 - attackBoxTop.Location.X + 25, attackBoxTop.Location.Y);
                    }

                    if (attackBoxBottom.Width <= 300)
                    {
                        attackBoxRight.Location = new Point(790, attackBoxTop.Location.Y);
                    }

                    if (attackBoxBottom.Location.X < 490)
                    {
                        attackBoxBottom.Location = new Point(attackBoxBottom.Location.X + 20, attackBoxBottom.Location.Y);
                        attackBoxTop.Location = new Point(attackBoxTop.Location.X + 20, attackBoxTop.Location.Y);
                        attackBoxLeft.Location = new Point(attackBoxTop.Location.X - 20 + 20, attackBoxTop.Location.Y);

                    }

                    if (attackBoxBottom.Width < 300)
                    {
                        attackBoxTop.Width = 300;
                        attackBoxBottom.Width = 300;
                    }

                    enemyShield = 5;
                    bulletAttackTimer.Interval = 600;
                    break;
                case 7:
                    afterAttackTimer.Interval = 4000;
                    afterAttackTimer.Start();
                    break;
                case 8:
                    attackBoxMove();
                    labelSelect1.Visible = true;
                    if (canSpare == 0)
                    {
                        labelSelect2.Visible = true;
                        labelSelect3.Visible = true;

                        labelSelect1.Text = Resources.buttonMark;
                        labelSelect2.Text = Resources.buttonCalm;
                        labelSelect3.Text = Resources.buttonReady;
                    }

                    labelSelect1.ForeColor = Color.White;
                    labelSelect2.ForeColor = Color.White;
                    labelSelect3.ForeColor = Color.White;
                    if (menuSelect == 0)
                    {
                        labelSelect1.ForeColor = Color.Yellow;
                    }
                    else if (menuSelect == 1)
                    {
                        labelSelect2.ForeColor = Color.Yellow;
                    }
                    else if (menuSelect == 2)
                    {
                        labelSelect3.ForeColor = Color.Yellow;
                    }
                    else if (menuSelect < 0)
                    {
                        menuSelect = 2;
                    }
                    else
                    {
                        menuSelect = 0;
                    }

                    if (calmCount >= 3 && readyCount >= 3 && canSpare == 0)
                    {
                        labelSelect1.ForeColor = Color.Red;
                        labelSelect1.Text = Resources.buttonSmile;
                    }
                    else if (canSpare != 0)
                    {
                        labelSelect1.ForeColor = Color.Yellow;
                        labelSelect1.Text = Resources.buttonSpare;
                        labelSelect2.Visible = true;
                        labelSelect3.Visible = true;
                        if (menuSelect != 0)
                        {
                            menuSelect = 0;
                        }
                    }
                    break;
                case 9:
                    break;
                case 10:
                    bulletAttackTimer.Stop();
                    gameAnimationTimer.Stop();
                    timer3.Stop();
                    bottleAttackTimer.Stop();
                    enemyBody.Dispose();
                    enemyHead.Dispose();
                    attackBoxLeft.Dispose();
                    attackBoxRight.Dispose();
                    attackBoxTop.Dispose();
                    attackBoxBottom.Dispose();
                    hpNameLabel.Dispose();
                    hpBar.Dispose();
                    fightButton.Dispose();
                    actButton.Dispose();
                    itemButton.Dispose();
                    labelGravity.Dispose();
                    foreach (Bullet b in bullets)
                    {
                        b.Dispose();
                    }
                    foreach (Bottle obj in objects)
                    {
                        this.Controls.Remove(obj.PictureBox);
                    }
                    afterAttackTimer.Dispose();
                    if (currentPlayingFile == 3)
                    {
                        playerHeart.Image = Properties.Resources.yheart;
                    }
                    else
                    {
                        playerHeart.Image = Properties.Resources.heart;
                    }
                    currentPlayingFile = -1;
                    musicPlayer.Stop();
                    playerHeart.BringToFront();
                    playerHeart.Location = new Point(616, 456);
                    deathTimer.Start();
                    GamePhase = 12;
                    break;
                case 11:
                    enemyBody.BackgroundImage = null;
                    enemyBody.Image = Resources.enemy_fall;
                    fightButton.Dispose();
                    actButton.Dispose();
                    itemButton.Dispose();
                    attackIndicator.Visible = false;
                    if (attackBox.Width > 0)
                    {
                        attackBox.Width -= 60;
                    }
                    afterAttackTimer.Interval = 3000;
                    afterAttackTimer.Start();
                    snapEffect1 = new ThanosSnapEffect(enemyBody);
                    snapEffect2 = new ThanosSnapEffect(enemyHead);
                    
                    break;
                case 12:
                    break;
                case 13:
                    enemyBody.BackgroundImage = null;
                    snapEffect1.Start();
                    snapEffect2.Start();
                    textBox.Visible = true;
                    afterAttackTimer.Interval = 5000;
                    afterAttackTimer.Start();
                    timer1.Stop();
                    break;
                case 14:
                    musicPlayer.Stop();
                    bulletAttackTimer.Stop();
                    gameAnimationTimer.Stop();
                    timer3.Stop();
                    bottleAttackTimer.Stop();
                    soundPlayer.Volume = 1;
                    soundPlayer.Stop();
                    soundPlayer.Init(deathEnemy);
                    soundPlayer.Play();
                    textBox.Visible = true;
                    textBox.Text = Resources.boxWin;
                    enemyBody.Image = Resources.enemySpared;
                    enemyHead.Image = Resources.enemyheadSpared;
                    fightButton.Dispose();
                    actButton.Dispose();
                    itemButton.Dispose();
                    timer1.Stop();
                    break;
            }
        }
    }
}
