﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class MenuForm : Form
    {
        private SoundPlayer menuPlayer = new SoundPlayer("select.wav");
        public MenuForm()
        {
            InitializeComponent();
        }

        private void startButton_MouseHover(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;
            startButton.ForeColor = Color.Yellow;
            menuPlayer.Play();
        }

        private void startButton_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            startButton.ForeColor = Color.White;
        }

        private void buttonEasy_CheckedChanged(object sender, EventArgs e)
        {
            menuPlayer.Play();
        }

        private void buttonHard_CheckedChanged(object sender, EventArgs e)
        {
            menuPlayer.Play();
        }

        private void buttonNormal_CheckedChanged(object sender, EventArgs e)
        {
            menuPlayer.Play();
        }

        private void boxInstaDeath_CheckedChanged(object sender, EventArgs e)
        {
            menuPlayer.Play();
        }

        public void startButton_Click(object sender, EventArgs e)
        {
            mainWindow form = new mainWindow();
            
            if (buttonEasy.Checked)
            {
                form.playerDefence = 1.33;
                form.attackBaseSpeed = 0.75;
                form.attackDelay = 0.8;
            }
            else if (buttonHard.Checked) 
            {
                form.playerDefence = 0.6;
                form.attackBaseSpeed = 1.2;
                form.attackDelay = 1.25;
            }
            else if (buttonNormal.Checked)
            {
                form.playerDefence = 1;
                form.attackBaseSpeed = 1;
                form.attackDelay = 1;
            }

            if (boxInstaDeath.Checked)
            {
                form.instaDeath = true;
            }

            form.ShowDialog(this);
        }

        private void labelExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
