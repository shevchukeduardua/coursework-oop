﻿using System;
using System.Drawing;
using System.Windows.Forms;

public class ThanosSnapEffect
{
    private Timer timer;
    private PictureBox pictureBox;
    private Random random;
    private float opacity;

    public ThanosSnapEffect(PictureBox pictureBox)
    {
        this.pictureBox = pictureBox;
        this.timer = new Timer();
        this.random = new Random();
        this.opacity = 1.0f;

        timer.Interval = 50;
        timer.Tick += new EventHandler(Timer_Tick);
    }

    public void Start()
    {
        timer.Start();
    }

    private void Timer_Tick(object sender, EventArgs e)
    {
        opacity -= 0.02f;

        ApplySnapEffect();

        if (opacity <= 0.0f)
        {
            timer.Stop();
            pictureBox.Visible = false;
        }
    }

    private void ApplySnapEffect()
    {
        if (opacity <= 0.0f)
        {
            pictureBox.Visible = false;
            return;
        }

        Bitmap bitmap = new Bitmap(pictureBox.Image);
        for (int y = 0; y < bitmap.Height; y++)
        {
            for (int x = 0; x < bitmap.Width; x++)
            {
                if (random.NextDouble() < (1.0 - opacity))
                {
                    bitmap.SetPixel(x, y, Color.Transparent);
                }
            }
        }

        pictureBox.Image = bitmap;
        pictureBox.Refresh();
    }
}
