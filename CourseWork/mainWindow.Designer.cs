﻿using System.Drawing;

namespace CourseWork
{
    partial class mainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainWindow));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gameAnimationTimer = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.textBox = new System.Windows.Forms.Label();
            this.enemyHpBar = new System.Windows.Forms.ProgressBar();
            this.hitAminTimer = new System.Windows.Forms.Timer(this.components);
            this.damageLabel = new System.Windows.Forms.Label();
            this.hpBar = new System.Windows.Forms.ProgressBar();
            this.hpNameLabel = new System.Windows.Forms.Label();
            this.afterAttackTimer = new System.Windows.Forms.Timer(this.components);
            this.playerHitTimer = new System.Windows.Forms.Timer(this.components);
            this.bulletAttackTimer = new System.Windows.Forms.Timer(this.components);
            this.cutsceneTimer = new System.Windows.Forms.Timer(this.components);
            this.bottleAttackTimer = new System.Windows.Forms.Timer(this.components);
            this.shootAttackTimer = new System.Windows.Forms.Timer(this.components);
            this.labelGravity = new System.Windows.Forms.Label();
            this.labelSelect2 = new System.Windows.Forms.Label();
            this.labelSelect3 = new System.Windows.Forms.Label();
            this.labelSelect1 = new System.Windows.Forms.Label();
            this.labelSelect4 = new System.Windows.Forms.Label();
            this.finalAttackTimer = new System.Windows.Forms.Timer(this.components);
            this.deathTimer = new System.Windows.Forms.Timer(this.components);
            this.shootFrame = new System.Windows.Forms.PictureBox();
            this.itemButton = new System.Windows.Forms.PictureBox();
            this.actButton = new System.Windows.Forms.PictureBox();
            this.fightButton = new System.Windows.Forms.PictureBox();
            this.enemyBody = new System.Windows.Forms.PictureBox();
            this.playerHeart = new System.Windows.Forms.PictureBox();
            this.attackBoxRight = new System.Windows.Forms.PictureBox();
            this.attackBoxLeft = new System.Windows.Forms.PictureBox();
            this.attackBoxTop = new System.Windows.Forms.PictureBox();
            this.attackBoxBottom = new System.Windows.Forms.PictureBox();
            this.enemyHead = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.shootFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fightButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyBody)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerHeart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyHead)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            // 
            // gameAnimationTimer
            // 
            this.gameAnimationTimer.Enabled = true;
            this.gameAnimationTimer.Interval = 63;
            this.gameAnimationTimer.Tick += new System.EventHandler(this.gameAnimationTimer_Tick);
            // 
            // timer3
            // 
            this.timer3.Enabled = true;
            this.timer3.Interval = 5000;
            this.timer3.Tick += new System.EventHandler(this.BodyAnim_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "label1";
            // 
            // textBox
            // 
            this.textBox.AutoSize = true;
            this.textBox.BackColor = System.Drawing.Color.Transparent;
            this.textBox.Font = new System.Drawing.Font("Determination Extended", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox.Location = new System.Drawing.Point(135, 492);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(0, 40);
            this.textBox.TabIndex = 12;
            // 
            // enemyHpBar
            // 
            this.enemyHpBar.ForeColor = System.Drawing.SystemColors.Desktop;
            this.enemyHpBar.Location = new System.Drawing.Point(143, 77);
            this.enemyHpBar.MarqueeAnimationSpeed = 250;
            this.enemyHpBar.Maximum = 2300;
            this.enemyHpBar.Name = "enemyHpBar";
            this.enemyHpBar.Size = new System.Drawing.Size(978, 23);
            this.enemyHpBar.Step = 1;
            this.enemyHpBar.TabIndex = 13;
            this.enemyHpBar.Value = 2300;
            this.enemyHpBar.Visible = false;
            // 
            // hitAminTimer
            // 
            this.hitAminTimer.Interval = 1700;
            this.hitAminTimer.Tick += new System.EventHandler(this.hitAminTimer_Tick);
            // 
            // damageLabel
            // 
            this.damageLabel.AutoSize = true;
            this.damageLabel.Font = new System.Drawing.Font("Determination Extended", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.damageLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.damageLabel.Location = new System.Drawing.Point(974, 35);
            this.damageLabel.Name = "damageLabel";
            this.damageLabel.Size = new System.Drawing.Size(86, 40);
            this.damageLabel.TabIndex = 14;
            this.damageLabel.Text = "123";
            this.damageLabel.Visible = false;
            // 
            // hpBar
            // 
            this.hpBar.Location = new System.Drawing.Point(615, 779);
            this.hpBar.Maximum = 40;
            this.hpBar.Name = "hpBar";
            this.hpBar.Size = new System.Drawing.Size(211, 23);
            this.hpBar.Step = 1;
            this.hpBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.hpBar.TabIndex = 15;
            this.hpBar.Value = 40;
            // 
            // hpNameLabel
            // 
            this.hpNameLabel.AutoSize = true;
            this.hpNameLabel.Font = new System.Drawing.Font("Determination Extended", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpNameLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.hpNameLabel.Location = new System.Drawing.Point(470, 780);
            this.hpNameLabel.Name = "hpNameLabel";
            this.hpNameLabel.Size = new System.Drawing.Size(140, 29);
            this.hpNameLabel.TabIndex = 16;
            this.hpNameLabel.Text = "HP 40/40";
            this.hpNameLabel.UseWaitCursor = true;
            // 
            // afterAttackTimer
            // 
            this.afterAttackTimer.Interval = 2000;
            this.afterAttackTimer.Tick += new System.EventHandler(this.afterAttackTimer_Tick);
            // 
            // playerHitTimer
            // 
            this.playerHitTimer.Interval = 1200;
            this.playerHitTimer.Tick += new System.EventHandler(this.playerHitTimer_Tick);
            // 
            // bulletAttackTimer
            // 
            this.bulletAttackTimer.Interval = 500;
            this.bulletAttackTimer.Tick += new System.EventHandler(this.bulletAttackTimer_Tick);
            // 
            // cutsceneTimer
            // 
            this.cutsceneTimer.Interval = 3000;
            this.cutsceneTimer.Tick += new System.EventHandler(this.cutsceneTimer_Tick);
            // 
            // bottleAttackTimer
            // 
            this.bottleAttackTimer.Interval = 2000;
            this.bottleAttackTimer.Tick += new System.EventHandler(this.bottleAttackTimer_Tick);
            // 
            // shootAttackTimer
            // 
            this.shootAttackTimer.Interval = 1000;
            this.shootAttackTimer.Tick += new System.EventHandler(this.shootAttackTimer_Tick);
            // 
            // labelGravity
            // 
            this.labelGravity.AutoSize = true;
            this.labelGravity.Font = new System.Drawing.Font("Determination Extended", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGravity.ForeColor = System.Drawing.Color.Yellow;
            this.labelGravity.Location = new System.Drawing.Point(161, 719);
            this.labelGravity.Name = "labelGravity";
            this.labelGravity.Size = new System.Drawing.Size(309, 29);
            this.labelGravity.TabIndex = 18;
            this.labelGravity.Text = "УВАГА: ЗИБУЧІ ПІСКИ";
            this.labelGravity.UseWaitCursor = true;
            this.labelGravity.Visible = false;
            // 
            // labelSelect2
            // 
            this.labelSelect2.AutoSize = true;
            this.labelSelect2.BackColor = System.Drawing.Color.Transparent;
            this.labelSelect2.Font = new System.Drawing.Font("Determination Extended", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelect2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSelect2.Location = new System.Drawing.Point(136, 579);
            this.labelSelect2.Name = "labelSelect2";
            this.labelSelect2.Size = new System.Drawing.Size(0, 40);
            this.labelSelect2.TabIndex = 19;
            // 
            // labelSelect3
            // 
            this.labelSelect3.AutoSize = true;
            this.labelSelect3.BackColor = System.Drawing.Color.Transparent;
            this.labelSelect3.Font = new System.Drawing.Font("Determination Extended", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelect3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSelect3.Location = new System.Drawing.Point(638, 492);
            this.labelSelect3.Name = "labelSelect3";
            this.labelSelect3.Size = new System.Drawing.Size(0, 40);
            this.labelSelect3.TabIndex = 20;
            // 
            // labelSelect1
            // 
            this.labelSelect1.AutoSize = true;
            this.labelSelect1.BackColor = System.Drawing.Color.Transparent;
            this.labelSelect1.Font = new System.Drawing.Font("Determination Extended", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelect1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSelect1.Location = new System.Drawing.Point(135, 492);
            this.labelSelect1.Name = "labelSelect1";
            this.labelSelect1.Size = new System.Drawing.Size(0, 40);
            this.labelSelect1.TabIndex = 21;
            // 
            // labelSelect4
            // 
            this.labelSelect4.AutoSize = true;
            this.labelSelect4.BackColor = System.Drawing.Color.Transparent;
            this.labelSelect4.Font = new System.Drawing.Font("Determination Extended", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelect4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSelect4.Location = new System.Drawing.Point(638, 579);
            this.labelSelect4.Name = "labelSelect4";
            this.labelSelect4.Size = new System.Drawing.Size(0, 40);
            this.labelSelect4.TabIndex = 22;
            // 
            // finalAttackTimer
            // 
            this.finalAttackTimer.Interval = 5000;
            this.finalAttackTimer.Tick += new System.EventHandler(this.finalAttackTimer_Tick);
            // 
            // deathTimer
            // 
            this.deathTimer.Interval = 1300;
            this.deathTimer.Tick += new System.EventHandler(this.deathTimer_Tick);
            // 
            // shootFrame
            // 
            this.shootFrame.ErrorImage = global::CourseWork.Properties.Resources.shoot5;
            this.shootFrame.Image = global::CourseWork.Properties.Resources.shoot5;
            this.shootFrame.Location = new System.Drawing.Point(582, 196);
            this.shootFrame.Name = "shootFrame";
            this.shootFrame.Size = new System.Drawing.Size(79, 77);
            this.shootFrame.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.shootFrame.TabIndex = 17;
            this.shootFrame.TabStop = false;
            this.shootFrame.Visible = false;
            // 
            // itemButton
            // 
            this.itemButton.BackColor = System.Drawing.Color.Transparent;
            this.itemButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.itemButton.Image = global::CourseWork.Properties.Resources.item_button;
            this.itemButton.Location = new System.Drawing.Point(790, 822);
            this.itemButton.Name = "itemButton";
            this.itemButton.Size = new System.Drawing.Size(161, 63);
            this.itemButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.itemButton.TabIndex = 10;
            this.itemButton.TabStop = false;
            this.itemButton.UseWaitCursor = true;
            // 
            // actButton
            // 
            this.actButton.BackColor = System.Drawing.Color.Transparent;
            this.actButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.actButton.Image = ((System.Drawing.Image)(resources.GetObject("actButton.Image")));
            this.actButton.Location = new System.Drawing.Point(558, 822);
            this.actButton.Name = "actButton";
            this.actButton.Size = new System.Drawing.Size(161, 63);
            this.actButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.actButton.TabIndex = 9;
            this.actButton.TabStop = false;
            this.actButton.UseWaitCursor = true;
            // 
            // fightButton
            // 
            this.fightButton.BackColor = System.Drawing.Color.Transparent;
            this.fightButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.fightButton.Image = global::CourseWork.Properties.Resources.fight_button;
            this.fightButton.Location = new System.Drawing.Point(334, 822);
            this.fightButton.Name = "fightButton";
            this.fightButton.Size = new System.Drawing.Size(161, 63);
            this.fightButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.fightButton.TabIndex = 8;
            this.fightButton.TabStop = false;
            this.fightButton.UseWaitCursor = true;
            // 
            // enemyBody
            // 
            this.enemyBody.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.enemyBody.BackColor = System.Drawing.Color.Transparent;
            this.enemyBody.BackgroundImage = global::CourseWork.Properties.Resources.sunset;
            this.enemyBody.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.enemyBody.Image = global::CourseWork.Properties.Resources.enemy_anim7;
            this.enemyBody.Location = new System.Drawing.Point(-15, 141);
            this.enemyBody.Name = "enemyBody";
            this.enemyBody.Size = new System.Drawing.Size(1319, 312);
            this.enemyBody.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.enemyBody.TabIndex = 6;
            this.enemyBody.TabStop = false;
            // 
            // playerHeart
            // 
            this.playerHeart.BackColor = System.Drawing.Color.Transparent;
            this.playerHeart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.playerHeart.Image = global::CourseWork.Properties.Resources.heart;
            this.playerHeart.Location = new System.Drawing.Point(627, 595);
            this.playerHeart.Name = "playerHeart";
            this.playerHeart.Size = new System.Drawing.Size(24, 24);
            this.playerHeart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerHeart.TabIndex = 4;
            this.playerHeart.TabStop = false;
            this.playerHeart.UseWaitCursor = true;
            // 
            // attackBoxRight
            // 
            this.attackBoxRight.BackColor = System.Drawing.Color.White;
            this.attackBoxRight.Location = new System.Drawing.Point(790, 459);
            this.attackBoxRight.Name = "attackBoxRight";
            this.attackBoxRight.Size = new System.Drawing.Size(5, 300);
            this.attackBoxRight.TabIndex = 3;
            this.attackBoxRight.TabStop = false;
            // 
            // attackBoxLeft
            // 
            this.attackBoxLeft.BackColor = System.Drawing.Color.White;
            this.attackBoxLeft.Location = new System.Drawing.Point(490, 459);
            this.attackBoxLeft.Name = "attackBoxLeft";
            this.attackBoxLeft.Size = new System.Drawing.Size(5, 300);
            this.attackBoxLeft.TabIndex = 2;
            this.attackBoxLeft.TabStop = false;
            // 
            // attackBoxTop
            // 
            this.attackBoxTop.BackColor = System.Drawing.Color.White;
            this.attackBoxTop.Location = new System.Drawing.Point(490, 459);
            this.attackBoxTop.Name = "attackBoxTop";
            this.attackBoxTop.Size = new System.Drawing.Size(300, 5);
            this.attackBoxTop.TabIndex = 1;
            this.attackBoxTop.TabStop = false;
            // 
            // attackBoxBottom
            // 
            this.attackBoxBottom.BackColor = System.Drawing.Color.White;
            this.attackBoxBottom.Location = new System.Drawing.Point(490, 754);
            this.attackBoxBottom.Name = "attackBoxBottom";
            this.attackBoxBottom.Size = new System.Drawing.Size(300, 5);
            this.attackBoxBottom.TabIndex = 0;
            this.attackBoxBottom.TabStop = false;
            // 
            // enemyHead
            // 
            this.enemyHead.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.enemyHead.BackColor = System.Drawing.Color.Transparent;
            this.enemyHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.enemyHead.Image = global::CourseWork.Properties.Resources.enemyhead;
            this.enemyHead.Location = new System.Drawing.Point(558, 21);
            this.enemyHead.Name = "enemyHead";
            this.enemyHead.Size = new System.Drawing.Size(168, 128);
            this.enemyHead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.enemyHead.TabIndex = 7;
            this.enemyHead.TabStop = false;
            // 
            // mainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1264, 921);
            this.Controls.Add(this.labelSelect4);
            this.Controls.Add(this.labelSelect1);
            this.Controls.Add(this.labelSelect3);
            this.Controls.Add(this.labelSelect2);
            this.Controls.Add(this.labelGravity);
            this.Controls.Add(this.shootFrame);
            this.Controls.Add(this.hpNameLabel);
            this.Controls.Add(this.hpBar);
            this.Controls.Add(this.damageLabel);
            this.Controls.Add(this.enemyHpBar);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.itemButton);
            this.Controls.Add(this.actButton);
            this.Controls.Add(this.fightButton);
            this.Controls.Add(this.enemyBody);
            this.Controls.Add(this.playerHeart);
            this.Controls.Add(this.attackBoxRight);
            this.Controls.Add(this.attackBoxLeft);
            this.Controls.Add(this.attackBoxTop);
            this.Controls.Add(this.attackBoxBottom);
            this.Controls.Add(this.enemyHead);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1280, 960);
            this.MinimumSize = new System.Drawing.Size(1280, 960);
            this.Name = "mainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BulletHell";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.shootFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fightButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyBody)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerHeart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBoxBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyHead)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox attackBoxBottom;
        private System.Windows.Forms.PictureBox attackBoxTop;
        private System.Windows.Forms.PictureBox attackBoxLeft;
        private System.Windows.Forms.PictureBox attackBoxRight;
        private System.Windows.Forms.PictureBox playerHeart;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox enemyBody;
        private System.Windows.Forms.Timer gameAnimationTimer;
        private System.Windows.Forms.PictureBox enemyHead;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.PictureBox fightButton;
        private System.Windows.Forms.PictureBox actButton;
        private System.Windows.Forms.PictureBox itemButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label textBox;
        private System.Windows.Forms.ProgressBar enemyHpBar;
        private System.Windows.Forms.Timer hitAminTimer;
        private System.Windows.Forms.Label damageLabel;
        private System.Windows.Forms.ProgressBar hpBar;
        private System.Windows.Forms.Label hpNameLabel;
        private System.Windows.Forms.Timer afterAttackTimer;
        private System.Windows.Forms.Timer playerHitTimer;
        private System.Windows.Forms.Timer bulletAttackTimer;
        private System.Windows.Forms.Timer cutsceneTimer;
        private System.Windows.Forms.Timer bottleAttackTimer;
        private System.Windows.Forms.PictureBox shootFrame;
        private System.Windows.Forms.Timer shootAttackTimer;
        private System.Windows.Forms.Label labelGravity;
        private System.Windows.Forms.Label labelSelect2;
        private System.Windows.Forms.Label labelSelect3;
        private System.Windows.Forms.Label labelSelect1;
        private System.Windows.Forms.Label labelSelect4;
        private System.Windows.Forms.Timer finalAttackTimer;
        private System.Windows.Forms.Timer deathTimer;
    }
}

